﻿namespace Wechat_PublicNumber.SettingModel
{
    public class CommonHttpSetting
    {
        public string GouldKey { get; set; }

        public string GouldBaseUrl { get; set; }

        public string TianKey { get; set; }

        public string TianBaseUrl { get; set; }

        public string XueQiuBaseUrl { get; set; }

        public string XueQiuStockBaseUrl { get; set; }
    }
}
