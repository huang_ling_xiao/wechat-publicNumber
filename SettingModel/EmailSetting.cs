﻿namespace Wechat_PublicNumber.SettingModel
{
    public class EmailSetting
    {
        public List<MailConfig> Item { get; set; }
    }

    public class MailConfig
    {
        /// <summary>
        /// 邮箱标识
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        ///  发件人邮箱地址
        /// </summary>
        public string Account { get; set; }
        /// <summary>
        ///  发件人别名
        /// </summary>
        public string Alias { get; set; }
        /// <summary>
        /// 发件人邮箱密码(或授权码)
        /// </summary>
        public string Pwd { get; set; }
        /// <summary>
        /// SMTP服务器地址
        /// </summary>
        public string ServerSMTP { get; set; }
        /// <summary>
        /// 端口
        /// </summary>
        public int PortSMTP { get; set; }

        /// <summary>
        /// IMAP服务器地址
        /// </summary>
        public string ServerIMAP { get; set; }
        /// <summary>
        /// IMAP服务器端口
        /// </summary>
        public int PortIMAP { get; set; }
    }
}
