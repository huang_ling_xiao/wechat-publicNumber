﻿namespace Wechat_PublicNumber.SettingModel
{
    public class WxSetting
    {
        public string Token { get; set; }

        public string AppID { get; set; }

        public string AppSecret { get; set; }

        public string AdminOpenID { get; set; }
    }
}
