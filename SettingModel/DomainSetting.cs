﻿namespace Wechat_PublicNumber.SettingModel
{
    [JsonFrom(Path = "appsettings.json")]
    public class DomainSetting
    {
        /// <summary>
        /// Api地址
        /// </summary>
        public string ApiDomain { get; set; }

        /// <summary>
        /// Web端地址
        /// </summary>
        public string WebDomain { get; set; }
    }
}
