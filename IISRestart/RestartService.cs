﻿using FastBuild.IISRestart;
using Wechat_PublicNumber.Common;
using Wechat_PublicNumber.Model;
using Wechat_PublicNumber.Repository;

namespace Wechat_PublicNumber.IISRestrat
{
    public class RestartService : IRestart
    {
        #region DI

        [Autowired]
        private WxHttpInvoke _wxHttpInvoke;

        [Autowired]
        private WxTemplateRepository _templateRepository;

        [Autowired]
        private ILog _logger;
        #endregion

        public async Task Execute()
        {
            var pushData = await _templateRepository.GetTemplateData(WeChatTemplateEnum.IISRestart, "");
            if (pushData is not null) await _wxHttpInvoke.PushTemplate(pushData);
            _logger.Info($"IIS already Restart", "IISRestart");
        }
    }
}
