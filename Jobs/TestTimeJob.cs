﻿using FastBuild.TimeJob;
using System.Diagnostics;
using Wechat_PublicNumber.Common;
using Wechat_PublicNumber.Entity;
using Wechat_PublicNumber.Repository;

namespace Wechat_PublicNumber.Jobs
{
    /// <summary>
    /// 测试定时任务
    /// </summary>
    [SkipWhileExecuting, ExecuteNow]
    public class TestTimeJob : DataAccess, IJob
    {
        /// <summary>
        /// 执行方法
        /// </summary>
        /// <returns></returns>
        public async Task Execute()
        {
            throw new NotImplementedException();
            Console.WriteLine($"{DateTime.Now}：TestTimeJob Already Execute");
        }
    }
}
