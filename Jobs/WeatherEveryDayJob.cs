﻿using FastBuild.TimeJob;
using Wechat_PublicNumber.Common;
using Wechat_PublicNumber.Model;
using Wechat_PublicNumber.Repository;

namespace Wechat_PublicNumber.Jobs
{
    /// <summary>
    /// 每日天气预报
    /// </summary>
    [SkipWhileExecuting]
    public class WeatherEveryDayJob : IJob
    {
        #region DI
        [Autowired]
        public ILog _logger;

        [Autowired]
        private WxHttpInvoke _wxHttpInvoke;

        [Autowired]
        private WxTemplateRepository _templateRepository;

        #endregion

        public async Task Execute()
        {
            _logger.Info("WeatherEveryDayJob Begin", "WeatherEveryDayJob");

            var openIdList = await _wxHttpInvoke.UserOpenIdList();
            var pushData = await _templateRepository.GetTemplateData(WeChatTemplateEnum.Weather, openIdList.data.openid);
            if (pushData is not null) await _wxHttpInvoke.PushTemplate(pushData);

            _logger.Info($"WeatherEveryDayJob End，Push Count:{openIdList.data.openid.Count}条", "WeatherEveryDayJob");
        }
    }
}
