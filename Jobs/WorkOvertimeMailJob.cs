﻿using FastBuild.TimeJob;
using SqlSugar;
using System.Text.RegularExpressions;
using Wechat_PublicNumber.Common;
using Wechat_PublicNumber.Entity;
using Wechat_PublicNumber.Model;
using Wechat_PublicNumber.Repository;
using Wechat_PublicNumber.SettingModel;

namespace Wechat_PublicNumber.Jobs
{
    /// <summary>
    /// 加班邮件监听
    /// </summary>
    [SkipWhileExecuting]
    public class WorkOvertimeMailJob : DataAccess, IJob
    {
        #region DI
        [Autowired]
        private readonly WxTemplateRepository _templateRepository;

        [Autowired]
        private readonly WxHttpInvoke _wxHttpInvoke;

        [Autowired]
        private readonly MailFactory mailFactory;

        [Autowired]
        private readonly WorkRepository _workRepository;

        [Autowired]
        private readonly WxSetting _wxSetting;
        #endregion

        private MailClient _mailClient = null;

        private EmailViewM _oneMail = null;

        public async Task Execute()
        {
            try
            {
                _logger.Info("WorkOvertimeMailJob Begin", "WorkOvertimeMailJob");

                _mailClient = mailFactory.CreateMailClient();

                var nowTime = DateTime.Now;

                var unReadMail = _mailClient.GetEmailByFolder();

                foreach (var item in unReadMail)
                {
                    _oneMail = _mailClient.GetEmailByUniqueId(item.UniqueId);

                    var userInfo = await WXDb.Queryable<Entity.UserInfo>().Where(s => s.Email == _oneMail.Address).FirstAsync();

                    string message = "本次加班：";
                    _logger.Info($"{_oneMail.Subject} 开始处理", "WorkOvertimeMailJob");

                    if (userInfo is not null && _oneMail.Subject.Contains("加班") && !string.IsNullOrEmpty(_oneMail.BodyText))
                    {
                        //开始处理
                        var jbData = MatchJbList(_oneMail.BodyText);

                        _logger.Info($"{_oneMail.Subject} 数据处理完成\nBody:\n{_oneMail.BodyText}\nData：{Newtonsoft.Json.JsonConvert.SerializeObject(jbData)}", "WorkOvertimeMailJob");

                        if (jbData is not null && jbData.Count > 0)
                        {
                            List<Work> workList = new List<Work>();
                            jbData = jbData.Where(s => s.Date >= DateTime.Parse($"{nowTime.Year}-01-01")).ToList();

                            foreach (var jb in jbData)
                            {
                                workList.Add(new Work()
                                {
                                    Hours = jb.Hour,
                                    OpenID = userInfo.OpenID,
                                    WhichDay = jb.Date,
                                    Type = WorkTypeEmnu.WorkOvertime,
                                    CreateTime = nowTime
                                });
                                message += $"{jb.Hour}h({jb.Date:yyyy-MM-dd})，";
                            }

                            //入库 并且 发送消息提醒
                            if (workList.Count > 0)
                            {
                                await WXDb.Insertable(workList).ExecuteCommandAsync();

                                var residueTime = await _workRepository.GetResidueWorkTime(userInfo.OpenID);

                                message += $"剩余{residueTime}h。";

                                var pushData = await _templateRepository.GetTemplateData(WeChatTemplateEnum.CommonMessage, userInfo.OpenID, new { Message = message });
                                if (pushData is not null) await _wxHttpInvoke.PushTemplate(pushData);

                                _logger.Info($"{_oneMail.Subject} 入库成功，Data：{Newtonsoft.Json.JsonConvert.SerializeObject(workList)}", "WorkOvertimeMailJob");
                            }
                        }
                    }
                    else if (_oneMail.Subject.Contains("安全提醒"))
                    {
                        //安全提醒
                        message = "wxadminp邮箱发生安全提醒：" + _oneMail.Subject;
                        var pushData = await _templateRepository.GetTemplateData(WeChatTemplateEnum.CommonMessage, _wxSetting.AdminOpenID, new { Message = message });
                        if (pushData is not null) await _wxHttpInvoke.PushTemplate(pushData);
                    }
                    //设置已读
                    _mailClient.SetMailFlag(item.UniqueId, MailKit.MessageFlags.Seen);
                    _logger.Info($"{_oneMail.Subject} 设置已读成功", "WorkOvertimeMailJob");
                }
                _logger.Info($"WorkOvertimeMailJob End", "WorkOvertimeMailJob");
            }
            catch
            {
                if (_oneMail != null)
                {
                    _mailClient.SetMailFlag(_oneMail.UniqueId, MailKit.MessageFlags.Flagged);
                    _mailClient.SetMailFlag(_oneMail.UniqueId, MailKit.MessageFlags.Seen);
                }
                throw;
            }
        }

        List<JbClass> MatchJbList(string text)
        {
            List<JbClass> jbClass = new List<JbClass>();

            text = text.Replace("：", ":").Replace(" ", "");

            var startIndex = text.IndexOf("时间:", 0);
            while (startIndex != -1)
            {
                if (startIndex == text.Length) break;

                var endIndex = text.IndexOf("时间:", startIndex + 1);
                if (endIndex == -1)
                    endIndex = text.Length;

                var tempText = text.Substring(startIndex, endIndex - startIndex);
                // 提取时间
                string timePattern = @"时间:(\d{4}-\d{1,2}-\d{1,2})";
                var time = ExtractValue(tempText, timePattern);

                // 提取小时
                string hoursPattern = @"小时:(\d+(\.\d)?)小时";
                var hour = ExtractValue(tempText, hoursPattern);

                jbClass.Add(new JbClass()
                {
                    Date = DateTime.Parse(time),
                    Hour = decimal.Parse(hour)
                });

                startIndex = endIndex;
            }

            return jbClass;
        }

        string ExtractValue(string input, string pattern)
        {
            Match match = Regex.Match(input, pattern);
            if (match.Success)
            {
                return match.Groups[1].Value;
            }
            else
            {
                return "未找到匹配项";
            }
        }

        public class JbClass
        {
            public DateTime Date { get; set; }

            public decimal Hour { get; set; }
        }
    }
}
