﻿using SqlSugar;
using Wechat_PublicNumber.Entity;
using Wechat_PublicNumber.Model;

namespace Wechat_PublicNumber.Repository
{
    public class HolidayRepository : DataAccess
    {
        /// <summary>
        /// 添加节假日
        /// </summary>
        /// <param name="entitys"></param>
        /// <returns></returns>
        public async Task AddHoliday(List<HolidayTime> entitys) => await WXDb.Insertable(entitys).ExecuteCommandAsync();

        /// <summary>
        /// 获取所有的节假日
        /// </summary>
        /// <returns></returns>
        public async Task<List<Holiday>> GetAllHoliday() => await WXDb.Queryable<Holiday>().ToListAsync();

        /// <summary>
        /// 根据ID获取假节日
        /// </summary>
        /// <param name="IDS"></param>
        /// <returns></returns>
        public async Task<List<Holiday>> GetHolidayByID(List<HolidayEnum> IDS)
        {
            return await WXDb.Queryable<Holiday>()
                .Where(s => IDS.Contains(s.ID))
                .ToListAsync();
        }
    }
}
