﻿using Wechat_PublicNumber.Model;

namespace Wechat_PublicNumber.Common
{
    [Injection(DIPattern.Singleton)]
    public class WxHttpInvoke
    {
        [Autowired]
        private ILog _logger;

        [Autowired]
        private WeChatHttpClinetHelper _weChatClinetHelper;

        /// <summary>
        /// 获取公众号用户OpenId数据
        /// </summary>
        /// <returns></returns>
        public async Task<WxPublicNumberUserList> UserOpenIdList()
        {
            var respone = await _weChatClinetHelper.Get<WxPublicNumberUserList>(Wechat_Url.UserOpenIdList_PublicNumber_Get, new string[] { "next_openid", "" });

            if (respone?.errcode == 0) return respone;
            return null;
        }

        /// <summary>
        /// 给公众号推送模板
        /// </summary>
        /// <returns></returns>
        public async Task PushTemplate(WxPushTemplate tempData)
        {
            if (tempData is null) return;
            await PushTemplate(new List<WxPushTemplate> { tempData });
        }

        /// <summary>
        /// 给公众号推送模板
        /// </summary>
        /// <param name="tempList"></param>
        /// <returns></returns>
        public async Task PushTemplate(List<WxPushTemplate> tempList)
        {
            foreach (var temp in tempList)
            {
                await _weChatClinetHelper.Post<WxPublicNumberPushTemplate>(Wechat_Url.PushTemplate_PublicNumber_Post, temp);
            }
        }

        /// <summary>
        /// 给用户设置remark 别名
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> UpdateRemarkToUser(WxUpdateRemarkToUserRequest model)
        {
            var respone = await _weChatClinetHelper.Post<WxUpdateRemarkToUserRespone>(Wechat_Url.UpdateRemarkToUser_PublicNumber_Post, model);

            if (respone?.errcode == 0) return true;

            return false;
        }

        /// <summary>
        /// 获取公众号全部用户
        /// </summary>
        /// <returns></returns>
        public async Task<List<WxUserInfoRespone>> UserList()
        {
            var openIdList = await UserOpenIdList();
            if (openIdList is null)
                return null;

            var userRequestItem = openIdList.data.openid.Select(s => new UserListRequestItem() { openid = s }).ToList();

            var respone = await _weChatClinetHelper.Post<WxUserListRespone>(Wechat_Url.UserList_PublicNumber_Post, new WxUserListRequest() { user_list = userRequestItem });

            if (respone?.errcode == 0) return respone.user_info_list;

            return null;
        }

        /// <summary>
        /// 获取公众号用户
        /// </summary>
        /// <param name="openId"></param>
        /// <returns></returns>
        public async Task<WxUserInfoRespone> UserInfo(string openId)
        {
            var respone = await _weChatClinetHelper.Get<WxUserInfoRespone>(Wechat_Url.UserInfo_PublicNumber_Get, new string[] { "openid", openId });

            if (respone?.errcode == 0) return respone;

            return null;
        }

        /// <summary>
        /// 创建标签
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> CreateTag(WxCreateTagRequest model)
        {
            var respone = await _weChatClinetHelper.Post<WxCreateTagRespone>(Wechat_Url.CreateTag_PublicNumber_Post, model);

            if (respone?.errcode == 0) return true;

            return false;
        }

        /// <summary>
        /// 上传临时文件
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public async Task<UploadMediaRespone> UploadMedia(string filePath, string fileName)
        {
            var respone = await _weChatClinetHelper.UploadMedia<UploadMediaRespone>(Wechat_Url.UploadMedia_Post, filePath, fileName, new string[] { "type", "image" });
            if (respone?.errcode == 0) return respone;

            return null;
        }

        /// <summary>
        /// 客服接口 - 发消息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> CustomSend(CustomSendRequest model)
        {
            var respone = await _weChatClinetHelper.Post<CustomSendRespone>(Wechat_Url.CustomSend_Post, model);
            if (respone?.errcode == 0) return true;
            return false;
        }

        /// <summary>
        /// 获取web端AccessToken
        /// </summary>
        /// <param name="appid"></param>
        /// <param name="secret"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        public async Task<AccessToken_Web_Respone> AccessToken_Web(string appid, string secret, string code)
        {
            var respone = await _weChatClinetHelper.Get<AccessToken_Web_Respone>(Wechat_Url.AccessToken_Web_Get, new string[] { "appid", appid, "secret", secret, "code", code, "grant_type", "authorization_code" }, false);
            if (respone?.errcode == 0) return respone;
            return null;
        }

        /// <summary>
        /// 刷新web端AccessToken
        /// </summary>
        /// <param name="appid"></param>
        /// <param name="refresh_token"></param>
        /// <returns></returns>
        public async Task<AccessToken_Web_Respone> RefreshToken_Web(string appid, string refresh_token)
        {
            var respone = await _weChatClinetHelper.Get<AccessToken_Web_Respone>(Wechat_Url.RefreshToken_Web_Get, new string[] { "appid", appid, "grant_type", "refresh_token", "refresh_token", refresh_token }, false);
            if (respone?.errcode == 0) return respone;
            return null;
        }

        /// <summary>
        /// web端获取用户的个人信息
        /// </summary>
        /// <param name="access_token"></param>
        /// <param name="openid"></param>
        /// <returns></returns>
        public async Task<UserInfo_Web_Respone> UserInfo_Web(string access_token, string openid)
        {
            var respone = await _weChatClinetHelper.Get<UserInfo_Web_Respone>(Wechat_Url.GetUserInfo_Web_Get, new string[] { "access_token", access_token, "lang", "zh_CN", "openid", openid }, false);
            if (respone?.errcode == 0) return respone;
            return null;
        }
    }
}
