﻿namespace Wechat_PublicNumber.Common
{
    public class WeChatReplyUserException : Exception
    {
        public WeChatReplyUserException() : base()
        {
        }

        public WeChatReplyUserException(string? message)
            : base(message)
        {
        }

    }
}
