﻿using Microsoft.AspNetCore.Authorization;
using Wechat_PublicNumber.Entity;

namespace Wechat_PublicNumber.Common
{
    [Injection(DIPattern.Scoped, InitMethod = nameof(Init))]
    public class UserAuthorization : DataAccess
    {
        #region DI
        [Autowired]
        private IGetToken _getToken;
        #endregion

        private void Init()
        {
            var toeknData = _getToken.GetToken<ToeknData>();
            if (toeknData != null)
                UserInfo = WXDb.Queryable<UserInfo>().Where(s => s.ID == toeknData.UserID).First();
        }

        public UserInfo UserInfo { get; set; }
    }

    public class ToeknData
    {
        public long UserID { get; set; }
    }
}
