﻿using SqlSugar;

namespace Wechat_PublicNumber.Common
{
    public class SqlSugarExternalFuc : SqlSugarExternalFucMethod
    {
        public List<SqlFuncExternal> GetexpMethods()
        {
            var expMethods = new List<SqlFuncExternal>();
            expMethods.Add(new SqlFuncExternal()
            {
                UniqueMethodName = "ISNull",
                MethodValue = (expInfo, dbType, expContext) =>
                {
                    return string.Format(@"{0} IS NULL",
                              expInfo.Args[0].MemberValue);
                }
            });
            return expMethods;
        }
    }
    public class SqlSugarExternalFucMethod
    {
        public static bool ISNull(string fileName) => throw new NotSupportedException("Can only be used in expressions");
    }
}
