﻿namespace Wechat_PublicNumber.Common
{
    [Injection(DIPattern.Scoped, InitMethod = nameof(Init))]
    public class Page
    {
        [Autowired]
        private IHttpContextAccessor _httpContextAccessor;

        private void Init()
        {
            try
            {
                if (_httpContextAccessor.HttpContext is not null)
                {
                    var query = _httpContextAccessor.HttpContext.Request.Query;
                    if (query is null) return;

                    int.TryParse(query["PageIndex"].FirstOrDefault(), out var pageIndex);
                    if (pageIndex != 0) this.pageIndex = pageIndex;
                    int.TryParse(query["PageSize"].FirstOrDefault(), out var pageSize);
                    if (pageSize != 0) this.pageSize = pageSize;
                }
            }
            catch { }
        }

        private int pageIndex = 1;
        public int PageIndex { get { return pageIndex; } }

        private int pageSize = 20;
        public int PageSize { get { return pageSize; } }
        public int TotalCount { get; set; }
    }
}
