﻿namespace Wechat_PublicNumber.Common
{
    public static class Wechat_Url
    {
        public const string BaseUrl = "https://api.weixin.qq.com";

        public static string Url(string url) => $"{BaseUrl}/{url}";

        /// <summary>
        /// 获取AccessToken
        /// </summary>
        public static string AccessToken_Get = Url("cgi-bin/token");

        /// <summary>
        /// 推送模板数据给用户
        /// </summary>
        public static string PushTemplate_PublicNumber_Post = Url("cgi-bin/message/template/send");

        /// <summary>
        /// 获取用户列表
        /// </summary>
        public static string UserOpenIdList_PublicNumber_Get = Url("cgi-bin/user/get");

        /// <summary>
        /// 获取用户信息
        /// </summary>
        public static string UserInfo_PublicNumber_Get = Url("cgi-bin/user/info");

        /// <summary>
        /// 批量获取用户信息
        /// </summary>
        public static string UserList_PublicNumber_Post = Url("cgi-bin/user/info/batchget");

        /// <summary>
        /// 设置用户备注名
        /// </summary>
        public static string UpdateRemarkToUser_PublicNumber_Post = Url("cgi-bin/user/info/updateremark");

        /// <summary>
        /// 创建标签
        /// </summary>
        public static string CreateTag_PublicNumber_Post = Url("cgi-bin/tags/create");

        /// <summary>
        /// 获取所有标签
        /// </summary>
        public static string GetTag_PublicNumber_Get = Url("cgi-bin/tags/get");

        /// <summary>
        /// 修改标签
        /// </summary>
        public static string UpdateTag_PublicNumber_Post = Url("cgi-bin/tags/update");

        /// <summary>
        /// 删除标签
        /// </summary>
        public static string DeleteTag_PublicNumber_Post = Url("cgi-bin/tags/delete");

        /// <summary>
        /// 获取标签下粉丝列表
        /// </summary>
        public static string GetUserByTag_PublicNumber_Post = Url("cgi-bin/user/tag/get");

        /// <summary>
        /// 批量为用户打标签
        /// </summary>
        public static string TaggingToUser_PublicNumber_Post = Url("cgi-bin/tags/members/batchtagging");

        /// <summary>
        /// 批量为用户取消标签
        /// </summary>
        public static string UnTaggingToUser_PublicNumber_Post = Url("cgi-bin/tags/members/batchuntagging");

        /// <summary>
        /// 上传临时素材
        /// </summary>
        public static string UploadMedia_Post = Url("cgi-bin/media/upload");

        /// <summary>
        /// 客服接口 - 发消息
        /// </summary>
        public static string CustomSend_Post = Url("cgi-bin/message/custom/send");

        #region 网页端相关接口
        /// <summary>
        /// 通过code换取网页授权access_token
        /// </summary>
        public static string AccessToken_Web_Get = Url("sns/oauth2/access_token");

        /// <summary>
        /// 刷新access_token（如果需要）
        /// </summary>
        public static string RefreshToken_Web_Get = Url("sns/oauth2/refresh_token");

        /// <summary>
        /// 拉取用户信息(需scope为 snsapi_userinfo)
        /// </summary>
        public static string GetUserInfo_Web_Get = Url("sns/userinfo");

        /// <summary>
        /// 检验授权凭证（access_token）是否有效
        /// </summary>
        public static string CheckAccessToken_Web_Get = Url("sns/auth");
        #endregion
    }
}
