﻿using Microsoft.AspNetCore.Mvc;
using SqlSugar;
using Wechat_PublicNumber.Common;
using Wechat_PublicNumber.Entity;

namespace Wechat_PublicNumber
{
    [Injection(DIPattern.Scoped)]
    public class BasicController : ControllerBase
    {
        [Autowired]
        private UserAuthorization _authorization;

        [Autowired]
        public ILog _logger;

        [Autowired]
        public DataAccess _dataAccess;

        [Autowired]
        public Page _page;
        public UserInfo UserInfo => _authorization.UserInfo;
    }
}
