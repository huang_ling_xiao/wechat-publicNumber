﻿using Wechat_PublicNumber.Entity;

namespace Wechat_PublicNumber.Common
{
    public static class StockTime
    {
        private static DateTime _date { get { return DateTime.Now.Date; } }

        /// <summary>
        /// 早盘开始时间
        /// </summary>
        public static DateTime MorningBeginTime { get { return _date.AddHours(9).AddMinutes(15); } }

        /// <summary>
        /// 早盘结束时间
        /// </summary>
        public static DateTime MorningEndTime { get { return _date.AddHours(11).AddMinutes(30); } }

        /// <summary>
        /// 午盘开始时间
        /// </summary>
        public static DateTime NoonBeginTime { get { return _date.AddHours(13); } }

        /// <summary>
        /// 午盘结束时间
        /// </summary>
        public static DateTime NoonEndTime { get { return _date.AddHours(15); } }

        private static bool _openingQuotation { get; set; }

        private static DateTime lastGetTime { get; set; }
        /// <summary>
        /// 是否开盘
        /// </summary>
        public static bool OpeningQuotation
        {
            get
            {
                if (lastGetTime != DateTime.Now.Date)
                {
                    _openingQuotation = (!ServiceContainer.GetService<DataAccess>().WXDb.Queryable<HolidayTime>().Where(s => s.Type == Model.HolidayTypeEnum.Holiday && s.Time == _date).Any()) && (_date.DayOfWeek!= DayOfWeek.Saturday || _date.DayOfWeek != DayOfWeek.Sunday);

                    lastGetTime = _date;
                }

                return _openingQuotation;
            }
        }
    }
}
