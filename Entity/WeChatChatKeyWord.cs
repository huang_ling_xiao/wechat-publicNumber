﻿using SqlSugar;
using Wechat_PublicNumber.Model;

namespace Wechat_PublicNumber.Entity
{
    public class WeChatChatKeyWord
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public long ID { get; set; }

        /// <summary>
        /// 关键字
        /// </summary>
        public string KeyWord { get; set; }

        /// <summary>
        /// 值
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 关键字对应的事件类型
        /// </summary>
        public ChatKeyWordEventTypeEnum? EventType { get; set; }

        public ChatKeyWordTypeEnum Type { get; set; }

        public string Remark { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
