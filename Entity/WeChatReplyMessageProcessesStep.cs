﻿using SqlSugar;
using Wechat_PublicNumber.Model;

namespace Wechat_PublicNumber.Entity
{
    public class WeChatReplyMessageProcessesStep
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public long ID { get; set; }

        /// <summary>
        /// 进程类型
        /// </summary>
        public ReplyMessageProcessesEnum ProcessesType { get; set; }

        public string StepName { get; set; }

        public string StepRemark { get; set; }

        public StepTypeEnum StepType { get; set; }

        public long NextStep { get; set; }

        public long SecondNextStep { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
