﻿using SqlSugar;
using Wechat_PublicNumber.Model;

namespace Wechat_PublicNumber.Entity
{
    /// <summary>
    /// 节假日
    /// </summary>
    public class Holiday
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public HolidayEnum ID { get; set; }

        /// <summary>
        /// 节假日名字
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public int OrderBy { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
