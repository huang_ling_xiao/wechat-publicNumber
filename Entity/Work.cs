﻿using SqlSugar;
using Wechat_PublicNumber.Model;

namespace Wechat_PublicNumber.Entity
{
    public class Work
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public long ID { get; set; }

        public string OpenID { get; set; }

        /// <summary>
        /// 几个小时
        /// </summary>
        public decimal Hours { get; set; }

        /// <summary>
        /// 哪一天
        /// </summary>
        public DateTime WhichDay { get; set; }

        /// <summary>
        /// 加班/补休/年假
        /// </summary>
        public WorkTypeEmnu Type { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
