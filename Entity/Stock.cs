﻿using SqlSugar;
using Wechat_PublicNumber.Model;

namespace Wechat_PublicNumber.Entity
{
    /// <summary>
    /// 用户股票信息设置
    /// </summary>
    public class Stock
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public long ID { get; set; }

        /// <summary>
        /// 股票Name
        /// </summary>
        public string StockName { get; set; }

        /// <summary>
        /// 股票编码
        /// </summary>
        public string StockCode { get; set; }

        /// <summary>
        /// 归属交易所
        /// </summary>
        public StockExchange BelongStockExchange { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
