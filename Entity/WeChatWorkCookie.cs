﻿using SqlSugar;

namespace Wechat_PublicNumber.Entity
{
    public class WeChatWorkCookie
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public long ID { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }

        public DateTime Dimport { get; set; }
    }
}
