﻿using SqlSugar;
using Wechat_PublicNumber.Model;

namespace Wechat_PublicNumber.Entity
{
    /// <summary>
    /// 事件表
    /// </summary>
    public class WeChatKeyWordEvent
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public long ID { get; set; }

        public long KeyWordID { get; set; }

        public int Proportion { get; set; }

        /// <summary>
        /// 关键字对应的事件类型
        /// </summary>
        public ChatKeyWordEventTypeEnum EventType { get; set; }

        /// <summary>
        /// 归属范围
        /// </summary>
        public int Type { get; set; }
    }
}
