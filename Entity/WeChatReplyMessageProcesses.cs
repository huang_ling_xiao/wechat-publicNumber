﻿using SqlSugar;
using Wechat_PublicNumber.Model;

namespace Wechat_PublicNumber.Entity
{
    /// <summary>
    /// 微信回复消息流程表
    /// </summary>
    public class WeChatReplyMessageProcesses
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public long ID { get; set; }

        /// <summary>
        /// 用户OpenID
        /// </summary>
        public string OpenID { get; set; }

        /// <summary>
        /// 当前进程类型
        /// </summary>
        public ReplyMessageProcessesEnum ProcessesType { get; set; }

        /// <summary>
        /// 当前进程阶段ID
        /// </summary>
        public long ProcessesStep { get; set; }

        /// <summary>
        /// 进程是否结束
        /// </summary>
        public bool ReplyEnd { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }
    }
}
