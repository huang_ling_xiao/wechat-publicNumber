﻿using SqlSugar;
using Wechat_PublicNumber.Model;

namespace Wechat_PublicNumber.Entity
{
    /// <summary>
    /// 节假日日期
    /// </summary>
    public class HolidayTime
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public long ID { get; set; }

        /// <summary>
        /// 外键
        /// </summary>
        public HolidayEnum HolidayID { get; set; }

        /// <summary>
        /// 时间
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public HolidayTypeEnum Type { get; set; }

        /// <summary>
        /// 年度
        /// </summary>
        public int Year { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
