﻿using SqlSugar;
using Wechat_PublicNumber.Model;

namespace Wechat_PublicNumber.Entity
{
    public class UserInfo
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public long ID { get; set; }

        public string OpenID { get; set; }

        public string Province { get; set; }

        public string City { get; set; }

        public string CityCode { get; set; }

        public string Area { get; set; }

        public string AreaCode { get; set; }

        /// <summary>
        /// 坐标点所在乡镇/街道
        /// 例如：燕园街道
        /// </summary>
        public string TownShip { get; set; }

        /// <summary>
        /// 附近地址
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 邮箱地址 目前手动维护
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 微信名称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 头像
        /// </summary>
        public string Headimgurl { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public Sex Sex { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime? UpdateTime { get; set; }
    }
}
