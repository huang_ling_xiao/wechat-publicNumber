﻿using SqlSugar;

namespace Wechat_PublicNumber.Entity
{
    public class WeChatWorkHistory
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public long ID { get; set; }

        public string SP_ID { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
