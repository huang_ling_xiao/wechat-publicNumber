﻿using SqlSugar;
using Wechat_PublicNumber.Model;

namespace Wechat_PublicNumber.Entity
{
    public class WorkYear
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public long ID { get; set; }

        public int Year { get; set; }

        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime BeginTime { get; set; }

        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime EndTime { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
