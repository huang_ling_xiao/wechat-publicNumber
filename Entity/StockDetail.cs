﻿using SqlSugar;

namespace Wechat_PublicNumber.Entity
{
    /// <summary>
    /// 用户股票信息设置
    /// </summary>
    public class StockDetail
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public long ID { get; set; }

        /// <summary>
        /// 行业归属
        /// </summary>
        public string industries { get; set; }

        /// <summary>
        /// 股票代码
        /// </summary>
        public string symbol { get; set; }

        /// <summary>
        /// 股票名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 当前价
        /// </summary>
        public decimal current { get; set; }

        /// <summary>
        /// 涨跌额
        /// </summary>
        public decimal chg { get; set; }

        /// <summary>
        /// 涨跌幅
        /// </summary>
        public decimal percent { get; set; }

        /// <summary>
        /// 年初至今
        /// </summary>
        public decimal current_year_percent { get; set; }

        /// <summary>
        /// 换手率
        /// </summary>
        public decimal turnover_rate { get; set; }

        /// <summary>
        /// 市盈率
        /// </summary>
        public decimal pe_ttm { get; set; }

        /// <summary>
        /// 60日内最高价到现在涨跌幅
        /// </summary>
        public decimal last_60_maxtocurrent_percent { get; set; }

        public decimal last_2_percent { get; set; }

        public decimal last_3_percent { get; set; }

        public decimal last_4_percent { get; set; }

        public decimal last_5_percent { get; set; }

        public decimal last_6_percent { get; set; }

        public decimal last_7_percent { get; set; }

        public decimal last_8_percent { get; set; }

        public decimal last_9_percent { get; set; }

        public decimal last_10_percent { get; set; }

        /// <summary>
        /// 命中状态
        /// </summary>
        public int? status { get; set; }

        /// <summary>
        /// 命中次数
        /// </summary>
        public int? count { get; set; }
    }
}
