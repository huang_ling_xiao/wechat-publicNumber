﻿using SqlSugar;

namespace Wechat_PublicNumber.Entity
{
    /// <summary>
    /// 待办事项
    /// </summary>
    public class ToDoList
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public long ID { get; set; }

        /// <summary>
        /// 待办事项名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 待办事项描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 提醒时间
        /// </summary>
        public DateTime ReminderTime { get; set; }

        /// <summary>
        /// 是否完成
        /// </summary>
        public bool IsDone { get; set; }

        public DateTime CreateTime { get; set; }
    }
}
