﻿using SqlSugar;
using Wechat_PublicNumber.Model;

namespace Wechat_PublicNumber.Entity
{
    /// <summary>
    /// 用户股票信息设置
    /// </summary>
    public class UserStock
    {
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public long ID { get; set; }

        /// <summary>
        /// 股票ID
        /// </summary>
        public long StockID { get; set; }

        /// <summary>
        /// OpenID
        /// </summary>
        public string OpenID { get; set; }

        /// <summary>
        /// 提醒——涨跌值
        /// </summary>
        public decimal RemindChg { get; set; }

        /// <summary>
        /// 提醒——百分比
        /// </summary>
        public decimal RemindPercent { get; set; }

        /// <summary>
        /// 提醒——价格
        /// </summary>
        public decimal RemindPrice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateTime { get; set; }

        public DateTime? UpdateTime { get; set; }
    }
}
