﻿using SqlSugar;
using Wechat_PublicNumber.Model;

namespace Wechat_PublicNumber.Entity
{
    public class WeChatTemplate
    {
        [SugarColumn(IsPrimaryKey = true)]
        public WeChatTemplateEnum ID { get; set; }

        /// <summary>
        /// 微信模板ID
        /// </summary>
        public string WeChatTemplateID { get; set; }

        /// <summary>
        /// 模板类型
        /// </summary>
        public WeChatTemplateTypeEnum Type { get; set; }

        /// <summary>
        /// OpenID
        /// </summary>
        public string OpenID { get; set; }

        /// <summary>
        /// ParameterID
        /// </summary>
        public int ParameterID { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime? UpdateTime { get; set; }
    }
}
