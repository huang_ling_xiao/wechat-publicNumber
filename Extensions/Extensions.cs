﻿using System.ComponentModel;
using System.Reflection;
using Newtonsoft.Json;

namespace Wechat_PublicNumber
{
    /// <summary>
    /// 拓展类
    /// </summary>
    public static class Extensions
    {
        #region T
        /// <summary>
        /// object To Class
        /// </summary>
        /// <typeparam name="TClass"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static TClass Convert<TClass>(this object obj) where TClass : class, new()
        {
            if (obj is null) return null;

            var objstring = JsonConvert.SerializeObject(obj);

            if (string.IsNullOrEmpty(objstring)) return null;

            return JsonConvert.DeserializeObject<TClass>(objstring);
        } 
        #endregion

        #region Enum
        /// <summary>
        /// 获取枚举注释
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescription(this Enum value)
        {
            var descriptionAttribute = ((TypeInfo)value.GetType()).DeclaredFields.Where(s => s.Name == value.ToString()).FirstOrDefault()
                    .GetCustomAttribute<DescriptionAttribute>();
            if (descriptionAttribute != null)
                return descriptionAttribute.Description;
            else
                return value.ToString();
        }
        #endregion

        #region decimal
        /// <summary>
        /// 一个值是否在自己的上下浮动范围之内
        /// </summary>
        /// <param name="value"></param>
        /// <param name="compareValue"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public static bool BetweenOffset(this decimal value, decimal compareValue, decimal offset)
        {
            return value >= compareValue - offset && value <= compareValue + offset;
        }

        /// <summary>
        /// 向右补全
        /// </summary>
        /// <param name="value"></param>
        /// <param name="integerLength">整数长度</param>
        /// <param name="paddingchar">填充char字符</param>
        /// <param name="keepNumber">保留小数</param>
        /// <returns></returns>
        public static string PaddingCharRigth(this decimal value, int integerLength, char paddingchar = ' ', int keepNumber = 0)
        {
            var temp = string.Empty;

            if (keepNumber == 0)
                temp = value.ToString();
            else
                temp = value.ToString($"0.{new string('0', keepNumber)}");

            var number = temp.Split('.')[0];

            var numberLength = temp.StartsWith("-") ? number.Length - 1 : number.Length;

            if (integerLength > numberLength)
                temp = $"{temp}{new string(paddingchar, (integerLength - numberLength) * 2)}";

            return temp;
        }
        #endregion

        #region DateTime
        /// <summary>
        ///  一个值是否在自己的上下浮动范围之内
        /// </summary>
        /// <param name="value"></param>
        /// <param name="compareValue"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        public static bool BetweenOffset(this DateTime value, DateTime compareValue, TimeSpan offset)
        {
            return value >= compareValue - offset && value <= compareValue + offset;
        } 
        #endregion
    }
}
