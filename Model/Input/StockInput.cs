﻿namespace Wechat_PublicNumber.Model.Input
{
    public class StockInput
    {
    }

    public class SaveStockInput
    {
        /// <summary>
        /// 
        /// </summary>
        public long ID { get; set; }

        /// <summary>
        /// 股票名称
        /// </summary>
        public string StockName { get; set; }

        /// <summary>
        /// 提醒——涨跌值
        /// </summary>
        public decimal RemindChg { get; set; }

        /// <summary>
        /// 提醒——百分比
        /// </summary>
        public decimal RemindPercent { get; set; }

        /// <summary>
        /// 提醒——价格
        /// </summary>
        public decimal RemindPrice { get; set; }
    }
}
