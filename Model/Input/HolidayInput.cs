﻿namespace Wechat_PublicNumber.Model.Input
{
    public class HolidayPostInput
    {
        /// <summary>
        /// 节假日
        /// </summary>
        public HolidayEnum Holiday { get; set; }

        /// <summary>
        /// 年度
        /// </summary>
        public int Year { get; set; }

        public List<HolidayPostItem> Item { get; set; }
    }

    public class HolidayPostItem
    {
        /// <summary>
        /// 节假日类型
        /// </summary>
        public HolidayTypeEnum HolidayType { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }

        public List<DateTime> Time { get; set; }
    }
}
