﻿namespace Wechat_PublicNumber.Model.Input.PublicNumberPushTemplate
{
    public class SupplementWeekdayTemplateInput
    {
        public string Holiday { get; set; }
    }

    public class CommonMessageTemplateInput
    {
        public string Message { get; set; }
    }

    public class StockWarningTemplateInput
    {
        public string stockName { get; set; }

        public decimal stockprice { get; set; }

        public decimal stockchg { get; set; }

        public decimal stockpercent { get; set; }
    }

    public class StockEndTemplateInput
    {
        public string stock1 { get; set; }

        public string stock2 { get; set; }

        public string stock3 { get; set; }

        public string stock4 { get; set; }

        public string stock5 { get; set; }
    }
}
