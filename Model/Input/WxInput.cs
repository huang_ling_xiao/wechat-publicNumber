﻿using FluentValidation;

namespace Wechat_PublicNumber.Model.Input
{
    public class WxPushTempInput
    {
        public WeChatTemplateEnum TemplateId { get; set; }

        public object Parm { get; set; }
    }

    public class WxPushTempByOpenIdInput : WxPushTempInput
    {
        public string OpenId { get; set; }
    }

    public class WxPushTempByRemarkInput : WxPushTempInput
    {
        public string Remark { get; set; }
    }


    public class FluentValidationTest1Input
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }

    public class FluentValidationTest1InputValidator : AbstractValidator<FluentValidationTest1Input>
    {
        public FluentValidationTest1InputValidator()
        {
            RuleFor(s => s.Key).Must((input, value, context) =>
            {
                if (string.IsNullOrEmpty(input.Value))
                    return false;
                return true;
            }).WithMessage("1");
        }
    }

    public class FluentValidationTest2Input
    {
        public string Key { get; set; }

        public string Value { get; set; }
    }

    public class FluentValidationTest2InputValidator : AbstractValidator<FluentValidationTest2Input>
    {
        public FluentValidationTest2InputValidator()
        {
            RuleFor(s => s.Key).Must((input, value, context) =>
            {
                if (string.IsNullOrEmpty(input.Value))
                    return false;
                return true;
            }).WithMessage("2");
        }
    }
}
