﻿using Newtonsoft.Json;

namespace Wechat_PublicNumber.Model
{
    public class WxUpdateRemarkToUserRequest
    {
        public string openid { get; set; }

        public string remark { get; set; }
    }

    public class WxUserListRequest
    {
        public List<UserListRequestItem> user_list { get; set; }
    }

    public class UserListRequestItem
    {
        public string openid { get; set; }

        public string lang { get; set; } = "zh_CN";
    }

    public class WxCreateTagRequest
    {
        public CreateTagRequestItem tag { get; set; }
    }

    public class CreateTagRequestItem
    {
        public string name { get; set; }
    }

    public class WxPushTemplate
    {
        public string touser { get; set; }
        public string template_id { get; set; }
        public object data { get; set; }

        [JsonIgnore]
        public string Description { get; set; }
    }
    public class ValueColor
    {
        public string value { get; set; }

        public string color { get; set; }
    }

    public class CustomSendRequest
    {
        public string touser { get; set; }

        public string msgtype { get; set; }

        public CustomSendRequest_text text { get; set; }

        public CustomSendRequest_image image { get; set; }
    }

    public class CustomSendRequest_text
    {
        public string content { get; set; }
    }

    public class CustomSendRequest_image
    {
        public string media_id { get; set; }
    }
}
