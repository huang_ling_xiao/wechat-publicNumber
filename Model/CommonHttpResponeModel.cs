﻿namespace Wechat_PublicNumber.Model
{
    public class GouldBaseRespone
    {
        public int status { get; set; }

        public string info { get; set; }
    }
    public class GouldAddressRespone : GouldBaseRespone
    {
        public GouldAddress_Regeocodes regeocode { get; set; }
    }

    public class GouldAddress_Regeocodes
    {
        public GouldAddress_AddressComponent addressComponent { get; set; }

        public string formatted_address { get; set; }
    }

    public class GouldAddress_AddressComponent
    {
        public object city { get; set; }
        public string province { get; set; }
        public string adcode { get; set; }
        public string district { get; set; }
        public string country { get; set; }
        public string township { get; set; }
        public string citycode { get; set; }
    }

    public class GouldWeatherRespone : GouldBaseRespone
    {
        /// <summary>
        /// 实况天气
        /// </summary>
        public List<GouldWeather_Lives> lives { get; set; }

        /// <summary>
        /// 预报天气信息数据
        /// </summary>
        public List<GouldWeather_Forecasts> forecasts { get; set; }
    }

    public class GouldWeather_Lives
    {

        public string province { get; set; }
        public string city { get; set; }
        public string adcode { get; set; }

        /// <summary>
        /// 天气现象（汉字描述）
        /// </summary>
        public string weather { get; set; }

        /// <summary>
        /// 实时气温，单位：摄氏度
        /// </summary>
        public string temperature { get; set; }

        /// <summary>
        /// 风向描述
        /// </summary>
        public string winddirection { get; set; }

        /// <summary>
        /// 风力级别，单位：级
        /// </summary>
        public string windpower { get; set; }

        /// <summary>
        /// 空气湿度
        /// </summary>
        public string humidity { get; set; }

        /// <summary>
        /// 数据发布的时间
        /// </summary>
        public string reporttime { get; set; }
    }

    public class GouldWeather_Forecasts
    {
        /// <summary>
        /// 城市名称
        /// </summary>
        public string city { get; set; }

        /// <summary>
        /// 城市编码
        /// </summary>
        public string adcode { get; set; }

        /// <summary>
        /// 省份名称
        /// </summary>
        public string province { get; set; }

        /// <summary>
        /// 预报发布时间
        /// </summary>
        public DateTime reporttime { get; set; }

        /// <summary>
        /// 预报数据list结构，元素cast,按顺序为当天、第二天、第三天的预报数据
        /// </summary>
        public List<GouldWeather_Forecasts_Casts> casts { get; set; }
    }

    public class GouldWeather_Forecasts_Casts
    {
        /// <summary>
        /// 日期
        /// </summary>
        public DateTime date { get; set; }

        /// <summary>
        /// 星期几
        /// </summary>
        public string week { get; set; }

        /// <summary>
        /// 白天天气现象
        /// </summary>
        public string dayweather { get; set; }

        /// <summary>
        /// 晚上天气现象
        /// </summary>
        public string nightweather { get; set; }

        /// <summary>
        /// 白天温度
        /// </summary>
        public string daytemp { get; set; }

        /// <summary>
        /// 晚上温度
        /// </summary>
        public string nighttemp { get; set; }

        /// <summary>
        /// 白天风向
        /// </summary>
        public string daywind { get; set; }

        /// <summary>
        /// 晚上风向
        /// </summary>
        public string nightwind { get; set; }

        /// <summary>
        /// 白天风力
        /// </summary>
        public string daypower { get; set; }

        /// <summary>
        /// 晚上风力
        /// </summary>
        public string nightpower { get; set; }
    }

    public class TianBaseRespone
    {
        public int code { get; set; }

        public string msg { get; set; }
    }

    public class TianWeatherRespone: TianBaseRespone
    {
        public List<TianWeatherItem> newslist { get; set; }
    }

    public class TianWeatherItem
    {
        /// <summary>
        /// 日期
        /// </summary>
        public string date { get; set; }

        /// <summary>
        /// 星期
        /// </summary>
        public string week { get; set; }

        /// <summary>
        /// 所属省份
        /// </summary>
        public string province { get; set; }

        /// <summary>
        /// 结果地区（市/区/县）
        /// </summary>
        public string area { get; set; }

        /// <summary>
        /// 城市天气ID
        /// </summary>
        public string areaid { get; set; }

        /// <summary>
        /// 实时天气（七天为早晚变化）
        /// </summary>
        public string weather { get; set; }

        /// <summary>
        /// 实时气温（七天仅为参考）
        /// </summary>
        public string real { get; set; }

        /// <summary>
        /// 最低温（夜间温度）
        /// </summary>
        public string lowest { get; set; }

        /// <summary>
        /// 最高温（日间温度）
        /// </summary>
        public string highest { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string tips { get; set; }
    }
}
