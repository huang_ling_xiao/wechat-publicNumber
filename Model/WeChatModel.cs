﻿namespace Wechat_PublicNumber.Model
{
    /// <summary>
    /// 微信回调验证model
    /// </summary>
    public class WeChatSignModel
    {
        /// <summary>
        /// 微信加密签名，signature结合了开发者填写的token参数和请求中的timestamp参数、nonce参数。
        /// </summary>
        public string Signature { get; set; }

        /// <summary>
        /// 时间戳
        /// </summary>
        public string Timestamp { get; set; }

        /// <summary>
        /// 随机数
        /// </summary>
        public string Nonce { get; set; }

        /// <summary>
        /// 随机字符串
        /// </summary>
        public string Echostr { get; set; }
        /// <summary>
        /// OpenId
        /// </summary>
        public string Openid { get; set; }
    }

    /// <summary>
    /// 微信XML基础返回格式
    /// </summary>
    public class WeChatXMLBaseModel
    {
        /// <summary>
        /// 小程序的username
        /// </summary>
        public string ToUserName { get; set; }

        /// <summary>
        /// 平台推送服务UserName
        /// </summary>
        public string FromUserName { get; set; }

        /// <summary>
        /// 发送时间
        /// </summary>
        public long CreateTime { get; set; }

        /// <summary>
        /// 默认为：Event
        /// </summary>
        public string MsgType { get; set; }

        /// <summary>
        /// 默认为：wxa_media_check
        /// </summary>
        public string Event { get; set; }
    }

    /// <summary>
    /// 微信地理位置
    /// </summary>
    public class WeChatLocationModel : WeChatXMLBaseModel
    {
        /// <summary>
        /// 地理位置纬度
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// 地理位置经度
        /// </summary>
        public string Longitude { get; set; }

        /// <summary>
        /// 地理位置精度
        /// </summary>
        public string Precision { get; set; }
    }

    /// <summary>
    /// 微信用户发送消息
    /// </summary>
    public class WeChatTextModel : WeChatXMLBaseModel
    {
        /// <summary>
        /// 消息内容
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 消息ID
        /// </summary>
        public string MsgId { get; set; }
    }

    /// <summary>
    /// 点击菜单
    /// </summary>
    public class WeChatClickMenuModel : WeChatXMLBaseModel
    {
        /// <summary>
        /// 菜单对应的Key
        /// </summary>
        public string EventKey { get; set; }
    }
}
