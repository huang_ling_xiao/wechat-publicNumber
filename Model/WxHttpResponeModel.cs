﻿namespace Wechat_PublicNumber.Model
{
    public class WxBaseRespone
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }

    public class WxPublicNumberUserList : WxBaseRespone
    {
        public int total { get; set; }
        public string count { get; set; }
        public WxPublicNumberUserList_data data { get; set; }
        public string next_openid { get; set; }
    }

    public class WxPublicNumberUserList_data
    {
        public List<string> openid { get; set; }
    }

    public class WxPublicNumberPushTemplate : WxBaseRespone
    {
        public long msgid { get; set; }
    }

    public class WxUpdateRemarkToUserRespone : WxBaseRespone
    {

    }

    public class WxUserListRespone : WxBaseRespone
    {
        public List<WxUserInfoRespone> user_info_list { get; set; }
    }

    public class WxUserInfoRespone : WxBaseRespone
    {
        /// <summary>
        /// 用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。
        /// </summary>
        public int subscribe { get; set; }

        public string openid { get; set; }

        public string language { get; set; }

        /// <summary>
        /// 用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
        /// </summary>
        public long subscribe_time { get; set; }

        public string unionid { get; set; }

        /// <summary>
        /// 公众号运营者对粉丝的备注
        /// </summary>
        public string remark { get; set; }

        /// <summary>
        /// 用户所在的分组ID
        /// </summary>
        public int groupid { get; set; }

        /// <summary>
        /// 用户被打上的标签 ID 列表
        /// </summary>
        public List<int> tagid_list { get; set; }

        /// <summary>
        /// 返回用户关注的渠道来源
        /// </summary>
        public string subscribe_scene { get; set; }
    }

    public class WxCreateTagRespone : WxBaseRespone
    {
        public CreateTagResponeItem tag { get; set; }
    }

    public class CreateTagResponeItem
    {
        public int id { get; set; }

        public string name { get; set; }
    }

    public class UserAddressRespone
    {
        /// <summary>
        /// 公众号运营者对粉丝的备注
        /// </summary>
        public string Remark { get; set; }

        public string Province { get; set; }

        public string City { get; set; }

        public string Area { get; set; }

        /// <summary>
        /// 坐标点所在乡镇/街道
        /// 例如：燕园街道
        /// </summary>
        public string TownShip { get; set; }
    }

    public class UploadMediaRespone : WxBaseRespone
    {
        public string type { get; set; }

        public string media_id { get; set; }

        public long created_at { get; set; }
    }

    public class CustomSendRespone : WxBaseRespone
    {

    }

    public class AccessToken_Web_Respone : WxBaseRespone
    {
        public string access_token { get; set; }

        public int expires_in { get; set; }

        public string refresh_token { get; set; }

        public string openid { get; set; }

        public string scope { get; set; }

        public int is_snapshotuser { get; set; }

        public string unionid { get; set; }
    }

    public class UserInfo_Web_Respone : WxBaseRespone
    {
        public string openid { get; set; }

        public string nickname { get; set; }

        public Sex sex { get; set; }

        public string province { get; set; }

        public string city { get; set; }

        public string country { get; set; }

        public string headimgurl { get; set; }

        public List<string> privilege { get; set; }

        public string unionid { get; set; }
    }
}
