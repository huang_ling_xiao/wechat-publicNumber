﻿using System.ComponentModel;

namespace Wechat_PublicNumber.Model
{
    /// <summary>
    /// 微信推送事件类型
    /// </summary>
    public enum WeChatEventEnum
    {
        [Description("地理位置")]
        LOCATION = 1,
        [Description("菜单点击")]
        CLICK = 2,
        [Description("关注")]
        subscribe = 3,
        [Description("二维码扫描")]
        SCAN = 4,
    }

    /// <summary>
    /// 微信模板
    /// </summary>
    public enum WeChatTemplateEnum
    {
        [Description("测试")]
        Test = 0,
        [Description("天气推送")]
        Weather = 1,
        [Description("股票收盘提醒")]
        StockEnd = 2,
        [Description("IIS重启")]
        IISRestart = 3,
        [Description("调休补班推送")]
        SupplementWeekday = 4,
        [Description("普通消息提醒")]
        CommonMessage = 5,
        [Description("股票预警提示")]
        StockWarning = 6
    }

    /// <summary>
    /// 假期相关性质类型
    /// </summary>
    public enum HolidayTypeEnum
    {
        [Description("假期")]
        Holiday = 1,

        [Description("补班")]
        SupplementWeekday = 2
    }

    /// <summary>
    /// 假期类型
    /// </summary>
    public enum HolidayEnum
    {
        [Description("元旦")]
        NewYear = 1,
        [Description("春节")]
        TraditionalNewYear = 2,
        [Description("清明")]
        QingmingFestival = 3,
        [Description("五一")]
        LaborFestival = 4,
        [Description("端午")]
        DragonBoatFestival = 5,
        [Description("中秋")]
        MidAutumnFestival = 6,
        [Description("国庆")]
        NationalFestival = 7,
    }

    /// <summary>
    /// 微信对话流程类型
    /// </summary>
    public enum ReplyMessageProcessesEnum
    {
        [Description("微信模板设置")]
        WeChatTemplateSet = 1
    }

    /// <summary>
    /// 微信对话流程阶段类型
    /// </summary>
    public enum StepTypeEnum
    {
        [Description("开始")]
        Begin = 1,
        [Description("进程中")]
        Inprocess = 2,
        [Description("结束")]
        End = 5
    }

    /// <summary>
    /// 微信模板类型
    /// </summary>
    public enum WeChatTemplateTypeEnum
    {
        [Description("全部")]
        All = 1,
        [Description("特殊")]
        Special = 2
    }

    /// <summary>
    /// 微信聊天关键字对应的事件类型
    /// </summary>
    public enum ChatKeyWordEventTypeEnum
    {
        [Description("正常聊天")]
        Chat = 1,
        [Description("设置微信模板")]
        WeChatTemplateSet = 2,
        [Description("获取微信模板")]
        WeChatTemplateGet = 3,
        [Description("获取加班记录")]
        WorkOvertimeGet = 4,
        [Description("设置加班记录")]
        WorkOvertimeSet = 5,
        [Description("设置个人股票监听")]
        StockSet = 6,
        [Description("获取帮助信息")]
        Helper = 7,
        [Description("重启定时任务")]
        ReStartTimeJob = 8,
        [Description("获取当前定时任务状态")]
        GetTimeJobStatus = 9
    }

    /// <summary>
    /// 工作类型
    /// </summary>
    public enum WorkTypeEmnu
    {
        [Description("加班")]
        WorkOvertime = 1,
        [Description("调休")]
        SupplementBreaks = 2,
        [Description("年假")]
        YearHoliday = 3,
        [Description("调年假")]
        SupplementYearHoliday = 4
    }

    public enum ChatKeyWordTypeEnum
    {
        [Description("无")]
        No = 0,
        [Description("禁用启用")]
        JQY = 1,
        [Description("全匹配")]
        AllGet = 2,
        [Description("Param")]
        Param = 3,
        [Description("使用Message")]
        UseMessage = 4
    }

    /// <summary>
    /// 股票交易所
    /// </summary>
    public enum StockExchange
    {
        [Description("深圳交易所")]
        SZ = 1,
        [Description("上海交易所")]
        SH = 2
    }

    /// <summary>
    /// 性别
    /// </summary>
    public enum Sex
    {
        [Description("未知")]
        None = 0,
        [Description("男")]
        Man = 1,
        [Description("女")]
        Women = 2
    }
}
