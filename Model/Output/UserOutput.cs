﻿namespace Wechat_PublicNumber.Model.Output
{
    public class UserOutput
    {
    }

    public class UserInfoOutput
    {
        /// <summary>
        /// 微信昵称
        /// </summary>
        public string WeChatNickName { get; set; }

        /// <summary>
        /// 微信头像
        /// </summary>
        public string WeChatHeadImgUrl { get; set; }
    }

    public class PushTemplateListOutput
    {
        /// <summary>
        /// 主键
        /// </summary>
        public WeChatTemplateEnum ID { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        public bool Status { get; set; }
    }
}
