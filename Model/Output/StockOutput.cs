﻿using Newtonsoft.Json;
using Wechat_PublicNumber.Common;

namespace Wechat_PublicNumber.Model.Output
{
    public class StockOutput
    {
    }

    public class GetStockList
    {
        /// <summary>
        /// 主键
        /// </summary>
        public long ID { get; set; }
        /// <summary>
        /// 提醒——涨跌值
        /// </summary>
        public decimal RemindChg { get; set; }

        /// <summary>
        /// 提醒——百分比
        /// </summary>
        public decimal RemindPercent { get; set; }

        /// <summary>
        /// 提醒——价格
        /// </summary>
        public decimal RemindPrice { get; set; }

        /// <summary>
        /// 股票Name
        /// </summary>
        public string StockName { get; set; }

        /// <summary>
        /// 股票编码
        /// </summary>
        [JsonIgnore]
        public string StockCode { get; set; }

        /// <summary>
        /// 归属交易所
        /// </summary>
        [JsonIgnore]
        public StockExchange BelongStockExchange { get; set; }
    }

    public class SearchStockOutput
    {
        public string Code { get; set; }

        public string Name { get; set; }
    }

    public class GetStockListOutput
    {
        public List<GetStockList> List { get; set; }

        public Page Page { get; set; }
    }
}
