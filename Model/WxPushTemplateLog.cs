﻿namespace Wechat_PublicNumber.Model
{
    public class WxPushTemplateLog
    {
        public string Description { get; set; }

        public object Respone { get; set; }

        public string ToUser { get; set; }

        public bool IsSuccess { get; set; }
    }
}
