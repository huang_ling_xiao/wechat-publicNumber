﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Wechat_PublicNumber.Entity;
using Wechat_PublicNumber.Model.Input;
using Wechat_PublicNumber.Repository;

namespace Wechat_PublicNumber.Controllers
{
    [Route("Holiday")]
    [ApiController]
    public class HolidayController : BasicController
    {

        #region DI
        [Autowired]
        private HolidayRepository _holidayRepsitory;

        #endregion

        /// <summary>
        /// 设置节假日
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task SetHoliday([FromBody] List<HolidayPostInput> input)
        {
            input.RemoveAll(s => !Enum.IsDefined(s.Holiday));

            var allHolidayType = await _holidayRepsitory.GetHolidayByID(input.Select(s => s.Holiday).ToList());

            var needAddHolidayType = allHolidayType.Select(s => s.ID).ToList();

            input.RemoveAll(s => !needAddHolidayType.Contains(s.Holiday));

            var nowtime = DateTime.Now;

            await _holidayRepsitory.AddHoliday(
                input.SelectMany(item =>
                {
                    if (item is null || (item.Year != 0 && item.Year > nowtime.Year + 1)) return new List<HolidayTime>();

                    return item.Item.SelectMany(s =>
                    {
                        if (s is null || ((s.Time is null || s.Time.Count <= 0) && (!s.StartTime.HasValue || !s.EndTime.HasValue))) return new List<HolidayTime>();

                        if (s.StartTime.HasValue)
                        {
                            if (s.EndTime.Value < s.StartTime.Value)
                                throw new OutPutException($"{(int)item.Holiday}({item.Holiday}) 开始时间大于结束时间");

                            return GetAllDate(s.StartTime.Value, s.EndTime.Value).Select(time => new HolidayTime()
                            {
                                CreateTime = nowtime,
                                HolidayID = item.Holiday,
                                Time = time,
                                Type = s.HolidayType,
                                Year = item.Year <= 0 ? nowtime.Year : item.Year,
                            }).ToList();
                        }

                        return s.Time.Select(time => new HolidayTime()
                        {
                            CreateTime = nowtime,
                            HolidayID = item.Holiday,
                            Time = time,
                            Type = s.HolidayType,
                            Year = item.Year <= 0 ? nowtime.Year : item.Year,
                        }).ToList();
                    }).ToList();
                }).ToList()
            );
        }

        /// <summary>
        /// 获取所有节假日
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<Holiday>> GetHoliday()
        {
            return await _holidayRepsitory.GetAllHoliday();
        }

        /// <summary>
        /// 获取开始和结束日期的所有Date
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        private List<DateTime> GetAllDate(DateTime start, DateTime end)
        {
            start = start.Date; end = end.Date;
            List<DateTime> result = new List<DateTime>();
            while (start <= end)
            {
                result.Add(start);
                start = start.AddDays(1);
            }
            return result;
        }
    }
}
