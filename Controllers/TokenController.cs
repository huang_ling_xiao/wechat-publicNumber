﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Wechat_PublicNumber.Common;

namespace Wechat_PublicNumber.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TokenController : BasicController
    {
        [Autowired]
        private IJwtHelper _jwtHelper;

        /// <summary>
        /// 获取Token
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, AllowAnonymous]
        public async Task<string> GetToken(long id)
        {
            return _jwtHelper.Encrypt(new ToeknData() { UserID = id });
        }

        /// <summary>
        /// 测试Token是否正确
        /// </summary>
        /// <returns></returns>
        [HttpGet("ValidateToken")]
        public async Task ValidateToken()
        {
        }
    }
}
