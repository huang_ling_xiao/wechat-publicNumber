using Newtonsoft.Json;
using System.Net;
using Wechat_PublicNumber.Common;
using Wechat_PublicNumber.Jobs.SustainJobs;
using Wechat_PublicNumber.Model;
using Wechat_PublicNumber.Repository;
using Wechat_PublicNumber.SettingModel;

FastBuilder.FastBuild(args,
(option, configuration) =>
{
    option.LogUniqueKey = (token) =>
    {
        return JsonConvert.DeserializeObject<ToeknData>(token).UserID.ToString();
    };

    option
    .UseJsonSetting()
    .UseTimeJob(s =>
    {
        // 错误处理
        s.ErrorHandler = async (serviceProvider, jobName, currentCount, jobStop) =>
        {
            var _templateRepository = serviceProvider.GetServiceWithAutowired<WxTemplateRepository>();
            var _wxHttpInvoke = serviceProvider.GetServiceWithAutowired<WxHttpInvoke>();
            var _wxSetting = serviceProvider.GetServiceWithAutowired<WxSetting>();

            var message = $"{jobName} 第{currentCount}次发生错误，正在重试...";
            if (jobStop)
                message = $"{jobName} 超过最大错误次数，已停止执行";

            var pushData = await _templateRepository.GetTemplateData(WeChatTemplateEnum.CommonMessage, _wxSetting.AdminOpenID, new { Message = message });
            if (pushData is not null) await _wxHttpInvoke.PushTemplate(pushData);
        };
    })
    .UseCros()
    .UseInjection(s =>
    {
        s.InjectionNamespace("{EntryProject}.Repository", DIPattern.Scoped);
    })
    .UseIISRestart(s =>
    {
        s.IISRestartTaskIpHost = "http://localhost:5666";
        s.ThisApplicationIpHost = configuration["ApiDomain"];
    })
    .UseGlobalRequestLog()
    .ConfigureServices(service =>
    {
        service.AddHttpClient();
        service.AddHttpClient("GetStock").ConfigurePrimaryHttpMessageHandler(s =>
        {
            return new HttpClientHandler()
            {
                AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
            };
        });
        service.AddMemoryCache();
    })
    .Configure(app =>
    {
        app.AddBackgroundService<StockMonitorJob>();
    });
});